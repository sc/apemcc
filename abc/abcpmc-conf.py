from sampler import *
from threshold import *
import numpy as np
import sys,os

from model.apemcc import CCSimu
from data.ceramic import *
from scipy.stats import ttest_ind_from_stats

from ecopy import mantel
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
from sklearn.metrics import confusion_matrix

import random



#distance function: sum of abs mean differences, we take Y (the evidenceS) as a list of percentage of the same sie than x but with value .95. _ie_ our evidence are a theoretical case where all the goods are at 95% of the same type during all the years
def dist(x, y):
    alldist=[]
    if len(x)<len(y['sd'].keys()):
        return(10000)
    for w in x.keys():
        allm=x[w].production["protruding_rim"]
        realmean=float(data['mean'][w]["protruding_rim"])
        realsd=float(data['sd'][w]["protruding_rim"])
        realsize=samplesize[w]
        sample=min(100,len(allm))
        lastm=allm[-sample:]
        simumean=np.mean(lastm)
        simusd=np.std(lastm)
        simusize=sample
        t,p=ttest_ind_from_stats(realmean,realsd,realsize,simumean,simusd,simusize,equal_var=False)
        alldist.append(1-p)
    return np.mean(alldist)

def manteldist(x, y):
    if x is None:
        return(1000)
    sites=[]
    metrics=np.empty((0,8))
    for w in x.keys() :
        wsid=x[w].wsid #get workshop id
        try: 
            last=x[w].production[len(x[w].production)-1]  #get last production
            sites=sites + [wsid]*len(last) #gie to all production the name of the workshop of the worker
            metrics=np.concatenate( (metrics,last) )
        except: 
            return(1000)
            #print(len(x[w].production))

    mat=confusion_matrix(sites,LDA(priors=[.2,.2,.2,.2,.2]).fit(metrics,sites).predict(metrics))
    mat[0,0]=mat[1,1]=mat[2,2]=mat[3,3]=mat[4,4]=0
    nil=mat.sum(1)==0 #store the perfect match to avoid nas
    mat[nil,]=[4,4,4,4,4]
    mat=1-(mat.T/mat.sum(1)).T
    for i in range(0,mat.shape[0]-1):
        for j in range(i+1,mat.shape[0]):
            mat[j,i]=mat[i,j]
    mat[0,0]=mat[1,1]=mat[2,2]=mat[3,3]=mat[4,4]=0
    robs=mantel.Mantel(d1=np.asarray(y["dist"]),d2=mat,tail='both',test='pearson').r_obs
    score=abs(y["robs"]-robs)
    #print(score)
    return (score)

#our "model", a gaussian with varying means
def postfn(theta):
    # we reject the particul with no credible parameters (ie pop < 0 etc...)
    #if(theta[0]>1 or theta[0]<0  or theta[2] < 3 or theta[2] > 50 or theta[1] > 10 or theta[1] < 0 or theta[3] > 80 or theta[3] < 5):
    if(theta[0]>10 or theta[0]<0):
        return(None)
    else:
        p_mu=0.05
        alpha=theta[0]
        time=250
        n_ws=20
        #prod_rate=int(theta[3])
        prod_rate=90
        #rate_depo=int(theta[4])
        #if(rate_depo==0):print("FUK DAT")
        ## we fixed the number of time step and we look only at three parameter: posize copy and mutation
        #exp=CCSimu(-1,time,pref,-1,p_mu,0,alpha,"file",dist_list=realdist,outputfile=False,mu_str=realsd,log=False,prod_rate=prod_rate,rate_depo=rate_depo)
        exp=CCSimu(n_ws,time,pref,-1,p_mu,alpha,"file",mu_str=allsds*2,rate_depo=time/10,outputfile=False,log=False)
        res=exp.run()
        return(res)



#Computing the real distances 
dataset = pd.read_csv('data/dataDressel.csv')
dataset= dataset[dataset['type'].isin(['Dressel C','Dressel D','Dressel E'])]
metrics = dataset.iloc[:, 4:12].values
sites = dataset.iloc[:, 12].values

distriv = pd.read_csv('data/riverDistances.csv',header=None) #reading rivers set distance
distriv=pd.crosstab(distriv.iloc[:,0],distriv.iloc[:,1],values=distriv.iloc[:,2],aggfunc="sum") #creating a distance matrix using the list of distance
distriv[distriv.isna()]=0 #replacing NA by 0

mat=confusion_matrix(sites,LDA(priors=[.2,.2,.2,.2,.2]).fit(metrics,sites).predict(metrics))

mat[0,0]=mat[1,1]=mat[2,2]=mat[3,3]=mat[4,4]=0
mat=1-(mat.T/mat.sum(1)).T
for i in range(0,mat.shape[0]-1):
    for j in range(i+1,mat.shape[0]):
        mat[j,i]=mat[i,j]
    
robs=mantel.Mantel(d1=np.asarray(distriv),d2=mat,tail='both',test='pearson',nperm=119).r_obs
robs=.51

data={"robs":robs,"dist":distriv}


pref=sys.argv[1] #a prefix that will be used as a folder to store the result of the ABC
mpi=True

### if use with MPI
if mpi : from  mpi_util import *



N=500
#
if mpi:
    mpi_pool = MpiPool()
    #print(mpi_pool.rank)
    #np.random.seed(seed=mpi_pool.rank)
    #seed = random.randrange(4294967295)
    #np.random.seed(seed=seed+mpi_pool.rank)      
    #np.random.seed(seed=seed)      
    eps = ExponentialEps(20,1, .01)
    #prior = TophatPrior([0,0,2,10],[.1,5,20,30])
    prior = TophatPrior([0],[5])
    #prior = TophatPrior([0,-1,15000,10,100],[1,1,45000,80,10000])
    sampler = Sampler(N=N, Y=data, postfn=postfn, dist=manteldist,pool=mpi_pool) 
else:
    eps = ExponentialEps(1,20, .001)
    prior = TophatPrior([0,0,200,50],[1,2,500,150])
    sampler = Sampler(N=N, Y=data, postfn=postfn, dist=manteldist)


#sampler.particle_proposal_cls = OLCMParticleProposal

if mpi and mpi_pool.isMaster() and not os.path.exists(pref):
    os.makedirs(pref)
if not mpi and not os.path.exists(pref):
    os.makedirs(pref)

if mpi and mpi_pool.isMaster() :
    with open(pref+"/ratio.txt", "a") as myfile:
        myfile.write("T,epsilon,ratio\n")
elif not mpi:
    with open(pref+"/ratio.txt", "a") as myfile:
        myfile.write("T,epsilon,ratio\n")

for pool in sampler.sample(prior, eps):
    if mpi  and mpi_pool.isMaster() :
        with open(pref+"/ratio.txt", "a") as myfile:
            myfile.write(str(pool.t)+","+str(pool.eps)+","+str(pool.ratio)+"\n")
    if not mpi : 
        print("T:{0},eps:{1:>.4f},ratio:{2:>.4f}".format(pool.t, pool.eps, pool.ratio))
        for i, (mean, std) in enumerate(zip(np.mean(pool.thetas, axis=0), np.std(pool.thetas, axis=0))):
            print(u"    theta[{0}]: {1:>.4f},{2:>.4f}".format(i, mean,std))
    #np.savetxt(pref+"/result_"+str(pool.eps)+".csv", pool.thetas, delimiter=",",header="p_mu,beta,time,prod_rate,rate_depo",fmt='%1.5f',comments="")
    np.savetxt(pref+"/result_"+str(pool.eps)+".csv", pool.thetas, delimiter=",",header="p_mu,beta,time",fmt='%1.5f',comments="")

print("DONE")
