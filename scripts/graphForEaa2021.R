prs <- function(d,alpha)exp(-alpha*d)/sum(exp(-alpha*d))
## nouvelle formule, ça fait ltp quon avance
alldata=lapply(list.files("rejectionABC/rejectionABC/",full.names = T),read.csv)
alldata=do.call("rbind.data.frame",alldata)
alldata=alldata[!is.na(as.numeric(alldata$alpha)),]
alldata=alldata[as.numeric(alldata$mu)==0.05,]
dim(alldata)
alldata$score=abs(as.numeric(as.character(alldata$r_obs))-0.21)
alldata=alldata[!is.nan(alldata$score),]
posterior=alldata#[alldata$p_val<0.2,]
bestDiff=posterior[posterior$diff<.01,]
bestScore=posterior[posterior$score<.1,]
mode=hdrcde::hdr(log10(bestDiff$alpha))$mode
mode=log10(as.numeric(names(which.max(table(bestDiff$alpha)))))

pdf("resultsABC.pdf",width=12)
par(mfrow=c(1,2))
boxplot(posterior$r_obs~posterior$alpha,ylab="mantel R",xlab=expression(alpha),main="Value of R for every alpha tested",xaxt="n")
axis(1,at=seq(1,length(unique(posterior$alpha)),length.out=6),label=sapply(lat,function(u)as.expression(bquote(10^.(u)))))
par(xpd=F)
abline(h=0.51,col="red")
#abline(h=0.51+0.01,col="red",lty=2)
#abline(h=0.51-0.01,col="red",lty=2)
par(xpd=T)
text(-10,0.51,"Coto-Sarmiento\n et al 2018",col="red",srt=45)

par(xpd=F)
plot(density(log10(bestDiff$alpha),from=-4,to=log10(4)),xlim=c(-4,1),col="green",ylab="",xlab=expression(alpha),xaxt="n",main="What alphas reproduce observed correlation",yaxt="n")
lat=c(-4,-3,-2,-1,0,1)
axis(1,at=lat,label=sapply(lat,function(u)as.expression(bquote(10^.(u)))))
lines(density(log10(alldata$alpha),from=-4,to=log10(4)))
#lines(density(log(bestScore$alpha),from=-9.21),col="red")

abline(v=mode,col="blue",lty=2) #the mode
mtext(bquote(alpha==.(round(10^mode,digit=2))), 3,0,at=mode,col="blue") #the mode
dev.off()

distances=read.csv("data/riverDistances.csv")

workcol=rainbow(length(unique(distances$from)))
names(workcol)=unique(distances$from)
    alphas=seq(-4,1,length.out=50)

pdf("probaForAllWorkshop.pdf",height=11,width=9)
par(mfrow=c(3,2))
for(refworkshop in unique(distances$from)){
    d_refworkshop=rbind.data.frame(cbind.data.frame(from=refworkshop,to=refworkshop,spatial=0),distances[distances$from == refworkshop,])
    probas=sapply(10^alphas,prs,d=d_refworkshop$spatial)
    rownames(probas)=as.character(unique(d_refworkshop$to))
    plot(alphas,probas[1,],type="n",ylim=c(0.001,1.5),log="y",xlab=expression(alpha),main=paste("Probability that workers from",refworkshop,"copy any other worker"),ylab="probability of copying",xaxt="n") 
    lat=seq(-4,1,1)
    axis(1,at=lat,label=sapply(lat,function(u)as.expression(bquote(10^.(u)))))
    for(w in rownames(probas)){
        lines(alphas,probas[w,],col=workcol[w],lwd=3)
        abline(v=mode,col="blue",lty=2) #the mode
        mtext(bquote(alpha==10^.(round(mode,digit=2))), 1,0.5,at=mode,col="blue") #the mode
    }
    legend("bottomright",legend=names(workcol),col=workcol,lwd=3)

}
dev.off()


pdf("probaForAllWorkshopMAXALPHA.pdf",height=11,width=9)
par(mfrow=c(3,2))
for(refworkshop in unique(distances$from)){
    d_refworkshop=rbind.data.frame(cbind.data.frame(from=refworkshop,to=refworkshop,spatial=0),distances[distances$from == refworkshop,])
    postproba=prs(d_refworkshop$spatial,10^mode)
    names(postproba)=d_refworkshop$to
    barplot(postproba,col=workcol[names(postproba)],legend=T,main=paste("proba. for worker from", refworkshop,"to copy when alpha=10^",round(mode,digits=2)))
}
dev.off()



pdf("probaForAllParlaMalpi.pdf",width=15,height=7)
par(mfrow=c(1,2))
for(refworkshop in c("parlamento","malpica")){
    d_refworkshop=rbind.data.frame(cbind.data.frame(from=refworkshop,to=refworkshop,spatial=0),distances[distances$from == refworkshop,])
    probas=sapply(10^alphas,prs,d=d_refworkshop$spatial)
    rownames(probas)=as.character(unique(d_refworkshop$to))
    plot(alphas,probas[1,],type="n",ylim=c(0.001,1.5),log="y",xlab=expression(alpha),main=paste("Probability that workers from",refworkshop,"\n copy any other worker"),ylab="probability of copying",xaxt="n") 
    lat=c(-6,-4,-2,0,2)
    axis(1,at=lat,label=sapply(lat,function(u)as.expression(bquote(10^.(u)))))
    for(w in rownames(probas)){
        lines(alphas,probas[w,],col=workcol[w],lwd=3)
        #abline(v=mode,col="blue",lty=2) #the mode
        #mtext(bquote(alpha==10^.(round(mode,digit=2))), 1,0.5,at=mode,col="blue") #the mode
    }
    legend("bottomleft",legend=names(workcol),col=workcol,lwd=3)

}
dev.off()


pdf("probaForAllWorkshopMAXALPHA.pdf",height=11,width=9)
par(mfrow=c(3,2))
for(refworkshop in unique(distances$from)){
    d_refworkshop=rbind.data.frame(cbind.data.frame(from=refworkshop,to=refworkshop,spatial=0),distances[distances$from == refworkshop,])
    postproba=prs(d_refworkshop$spatial,10^mode)
    names(postproba)=d_refworkshop$to
    barplot(postproba,col=workcol[names(postproba)],legend=T,main=paste("proba. for worker from", refworkshop,"\n to copy when alpha=",round(10^mode,digits=2)),log="y",ylim=c(10^-6,2),xpd=F)
}
dev.off()

pdf("probaForAllWorkshopMAXALPHA_parlmalpi.pdf",height=7,width=12)
par(mfrow=c(1,2))
for(refworkshop in c("parlamento","malpica")){
    d_refworkshop=rbind.data.frame(cbind.data.frame(from=refworkshop,to=refworkshop,spatial=0),distances[distances$from == refworkshop,])
    postproba=prs(d_refworkshop$spatial,10^mode)
    names(postproba)=d_refworkshop$to
    barplot(postproba,col=workcol[names(postproba)],legend=T,main=paste("proba. for worker from", refworkshop,"\n to copy when alpha=",round(10^mode,digits=2)),log="y",ylim=c(10^-6,2),xpd=F)
    #mtext(1,0,   =names(postproba), srt = 45, adj = c(1.1,1.1), xpd = TRUE, cex=0.6)
text(seq(1.5, length(names(postproba)), by = 1), par("usr")[3]-0.25, 
     srt = 60, adj = 1, xpd = TRUE,
     labels = paste(names(postproba)), cex = 0.65)
}
dev.off()


