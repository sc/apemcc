
# Import the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# Import the dataset
dataset = pd.read_csv('data/dataDressel.csv')
metrics = dataset.iloc[:, 4:12].values
sites = dataset.iloc[:, 12].values

distriv = pd.read_csv('data/riverDistances.csv') #reading rivers set distance
distriv=pd.crosstab(distriv.iloc[:,0],distriv.iloc[:,1],values=distriv.iloc[:,2],aggfunc="sum") #creating a distance matrix using the list of distance
distriv[distriv.isna()]=0 #replacing NA by 0


# Splitting the dataset into the Training set and Test set
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(metrics, sites, test_size = 0.2)

# Feature Scaling to Dataset
from sklearn.preprocessing import StandardScaler
sc = StandardScaler()
X_train = sc.fit_transform(X_train)
X_test = sc.transform(X_test)

# Implement LDA
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
lda = LDA(n_components = 1)
X_train = lda.fit_transform(metrics, sites)
X_test = lda.transform(metrics)

# Train Logistic Regression with LDA
from sklearn.linear_model import LogisticRegression
classifier = LogisticRegression()
classifier.fit(X_train, y_train)

# Predict Results of Regression with LDA
y_pred = classifier.predict(X_test)

# Confusion Matrix 3X3
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)
print(cm)

#onliner
dataset = pd.read_csv('data/dataDressel.csv')
dataset= dataset[dataset['type'].isin(['Dressel C','Dressel D','Dressel E'])]
metrics = dataset.iloc[:, 4:12].values
sites = dataset.iloc[:, 12].values

distriv = pd.read_csv('data/riverDistances.csv',header=None) #reading rivers set distance
distriv=pd.crosstab(distriv.iloc[:,0],distriv.iloc[:,1],values=distriv.iloc[:,2],aggfunc="sum") #creating a distance matrix using the list of distance
distriv[distriv.isna()]=0 #replacing NA by 0

mat=confusion_matrix(sites,LDA(priors=[.2,.2,.2,.2,.2]).fit(metrics,sites).predict(metrics))

mat[0,0]=mat[1,1]=mat[2,2]=mat[3,3]=mat[4,4]=0
mat=1-(mat.T/mat.sum(1)).T
for i in range(0,mat.shape[0]-1):
    for j in range(i+1,mat.shape[0]):
        mat[j,i]=mat[i,j]
    



from ecopy import mantel
mantel.Mantel(d1=np.asarray(distriv),d2=matDist,tail='both',test='pearson',nperm=119).r_obs

